<?php
use Illuminate\Support\Facades\Route;

use App\Transaction;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
 
Route::get('/', function () {
    return view('mainpage');
});
Auth::routes();

Route::resource('/profile','ProfileController');
Route::resource('/product','ProductController');
Route::resource('/topupcoin','TopUpCoinController');

Route::get('/buyhistory', function () {
    $buyhistory = Transaction::where('buyer',Auth::user()->user_id)->get();
    return view( 'buyhistory',compact('buyhistory'));
});

Route::get('/salehistory',function(){
    $salehistory = Transaction::where('seller', Auth::user()->user_id)->get();
    return view( 'salehistory', compact('salehistory'));
});

