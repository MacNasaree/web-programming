@extends('layouts.app2hand')

@section('content')
<br><br><br>
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <h3>เติม Coin</h3>
            </div>
            <br>
            <div class="card-body">
                @if (\Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ \Session::get('success') }}</p>
                </div>
                @endif
                <form enctype="multipart/form-data" class="confirm" action="{{ url('/topupcoin') }}" method="POST">
                    @csrf

                    <div class="form-group row">
                        <label for="coin" class="col-md-4 col-form-label text-md-right">{{ __('จำนวน Coin') }}</label>
                        <div class="col-md-6">
                            <input id="coin" type="text" class="form-control{{ $errors->has('coin') ? ' is-invalid' : '' }}" name="coin" value="{{ old('coin') }}" required>
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                ตกลง
                            </button>
                            <a href="/product" class="btn btn-primary">ยกเลิก</a>
                        </div>
                    </div>

                </form> <!-- End Form -->


            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('.confirm').on('submit', function() {
            if (confirm('ยืนยันการเติม')) {
                return true;
            } else {
                return false;
            }
        });
    });
</script>



@endsection