@extends('layouts.app2hand')

@section('content')
<br><br><br><br>
<style>
    #customers {
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    #customers td,
    #customers th {
        border: 1px solid #ddd;
        padding: 8px;
    }

    #customers tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    #customers tr:hover {
        background-color: #ddd;
    }

    #customers th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #4CAF50;
        color: white;
    }
</style>

<div class="container">
    @if (count($buyhistory) > 0)
        <h1 align="center">สินค้าที่คุณซื้อ</h1><br>
        <table id="customers">
        <tr>
            <th>หมวดหมู่</th>
            <th>ราคา</th>
            <th>ผู้ขาย</th>
            <th>ซื้อเมื่อวันที่</th>
        </tr>
        @foreach($buyhistory as $data)
        <tr>
            <td>{{ $data->category }}</td>
            <td>{{ $data->price }}</td>
            <td>{{ App\Profile::where('user_id',$data->seller)->first()->first_name }} {{ App\Profile::where('user_id',$data->seller)->first()->last_name }}</td>
            <td>{{ $data->created_at }}</td>
        </tr>
        @endforeach
    </table>
    @else
    <br><br><br>
    <h1 align="center">ไม่มีประวัติการซื้อ</h1>
    @endif
</div>




@endsection