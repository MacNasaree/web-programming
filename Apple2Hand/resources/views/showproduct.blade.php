@extends('layouts.app2hand')

@section('content')
<br>
<br>
<br>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h3>รายละเอียดสินค้า {{ $product->category }}</h3>
                </div>

                <div class="card-body">
                    <img src="/uploads/product_image/{{ $product->image }}" style="width:50%;">
                    <hr>
                    <h5>หมายเลขสินค้า : {{ $product->product_id }}</h5>
                    <h1>{{ $product->title }}</h1><br>
                    <h3>รายละเอียดสินค้า</h3>
                    <h4>- {{ $product->description }}</h4><br>
                    <h3 style="color:#FF5B33;">ราคา : {{ $product->price }} บาท</h3><br>

                    @auth
                    @if ($product->user_id != Auth::user()->user_id)
                    <form class="check" action="{{ action('ProductController@destroy',$product->product_id) }}" method="post">
                        @csrf
                        <input type="hidden" name="_method" value="DELETE">
                        <button type="submit" class="btn btn btn-warning">&emsp;ซื้อ &emsp;</button><br><br>
                    </form>
                    @else
                    <form class="check" action="{{ action('ProductController@edit',$product->product_id) }}" method="get">
                        @csrf
                        <button type="submit" class="btn btn btn-danger">ลบ</button><br><br>
                    </form>
                    @endif
                    @endauth

                    @guest
                    <a href="/login" class="btn btn-warning"> &emsp;ซื้อ&emsp;</button>&emsp;
                        @endguest

                        <a href="/product"><button type="button" class="btn btn-primary">ย้อนกลับ</button></a><br><br>
                        <hr>
                        <h3>ผู้ขาย : {{ App\Profile::where('user_id',$product->user_id)->first()->first_name }} {{ App\Profile::where('user_id',$product->user_id)->first()->last_name }}
                        </h3>
                        <h5>--- ติดต่อผู้ขาย ---</h5>
                        <h6><i class='fas fa-phone-volume' style='font-size:32px;color:red'></i> เบอร์โทร : {{ App\Profile::where('user_id',$product->user_id)->first()->tel }}</h6>
                        <h6><i class='fab fa-facebook' style='font-size:32px;color:blue'></i> Facebook : {{ App\Profile::where('user_id',$product->user_id)->first()->facebook }}</h6>
                        <h6><i class='fab fa-line' style='font-size:32px;color:green'></i> Line ID : {{ App\Profile::where('user_id',$product->user_id)->first()->line }}</h6>
                        <hr>
                        <p>ลงขายเมื่อ : {{ $product->created_at }}</p>
                        <p>อัพเดทล่าสุดเมื่อ : {{ $product->created_at }}</p>
                        <br>
                        <br>
                        <hr>
                </div> <!-- -->


            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('.check').on('submit', function() {
            if (confirm('ต้องการทำรายการต่อหรือไม่')) {
                return true;
            } else {
                return false;
            }
        });
    });
</script>


@endsection