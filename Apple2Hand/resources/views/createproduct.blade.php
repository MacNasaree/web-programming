@extends('layouts.app2hand')

@section('content')
<br>
<br>
<br>
<br>
<!-- <h1 align="center">ลงขายสินค้า</h1> -->

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h3>ลงขายสินค้า</h3>
                </div>
                <br>
                <div class="card-body">
                    <form enctype="multipart/form-data" action="{{ url('/product') }}" method="POST">
                        @csrf

                        <div class="form-group row">
                            <label for="category" class="col-md-4 col-form-label text-md-right">{{ __('หมวดหมู่') }}</label>
                            <div class="col-md-6">
                                <select id="category" name="category">
                                    <option name="category" value="macbook">Macbook</option>
                                    <option name="category" value="ipad">Ipad</option>
                                    <option name="category" value="iphone">Iphone</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="image" class="col-md-4 col-form-label text-md-right">{{ __('รูปภาพ') }}</label>
                            <div class="col-md-6">
                                <input type="file" name="image" id="image">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="title" class="col-md-4 col-form-label text-md-right">{{ __('Title') }}</label>
                            <div class="col-md-6">
                                <input id="title" type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" value="{{ old('title') }}" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="description" class="col-md-4 col-form-label text-md-right">{{ __('รายละเอียด') }}</label>
                            <div class="col-md-6">
                                <textarea id="description" type="text" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" value="{{ old('description') }}" required rows="7" cols="50" maxlength="250"></textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="price" class="col-md-4 col-form-label text-md-right">{{ __('ราคา') }}</label>
                            <div class="col-md-6">
                                <input id="price" type="text" class="form-control{{ $errors->has('price') ? ' is-invalid' : '' }}" name="price" value="{{ old('price') }}" required>
                                @if ($errors->has('price'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('price') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    ลงขายสินค้า
                                </button>
                                <a href="/product" class="btn btn-primary">ยกเลิก</a>
                            </div>
                        </div>

                    </form> <!-- End Form -->


                </div>
            </div>
        </div>
    </div>
</div>

@endsection