@extends('layouts.app2hand')

@section('content')
<br>
<br>

<main role="main">


    <div class="album py-5 bg-light">
        <div class="container">

            <div class="row">
                <!-- Start -->

                @if (count($product) > 0)

                @foreach ($product as $data)
                <div class="col-md-3">
                    <div class="card mb-4 shadow-sm">

                        <img src="/uploads/product_image/{{ $data->image }}" width="100%">

                        <div class="card-body">
                            <h5>{{ $data->category }}</h5>
                            @if ( strlen($data->title) > 15)
                            <p class="card-text">{{ substr($data->title,0,20) }}.....</p>
                            @else
                            <p class="card-text">{{ $data->title }}</p>
                            @endif
                            <h6 style="color:#FF5B33;">ราคา : {{ $data->price }} บาท</h6>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="btn-group">
                                    <a href="{{ action('ProductController@show',$data->product_id) }}" class="btn btn-sm btn-danger">เพิ่มเติม</a>
                                    @auth
                                    @if ($data->user_id != Auth::user()->user_id)
                                    <form class="check" action="{{ action('ProductController@destroy',$data->product_id) }}" method="post">
                                        @csrf
                                        <input type="hidden" name="_method" value="DELETE">
                                        <button type="submit" class="btn btn-sm btn-warning">ซื้อ</button>
                                    </form>
                                    @endif
                                    @else
                                    <a href="/login" class="btn btn-sm btn-warning">ซื้อ</a>
                                    @endauth
                                </div>
                            </div>
                            <h6 class="text-muted">ผู้ขาย<a href="{{ action('ProfileController@show',$data->user_id) }}"> {{ App\Profile::where('user_id',$data->user_id)->first()->first_name }} {{ App\Profile::where('user_id',$data->user_id)->first()->last_name }}</a> </h6>
                        </div>
                    </div>
                </div>
                @endforeach

                @else
                <h1 align="center">ไม่มีสินค้าที่ลงขาย</h1>
                @endif

            </div> <!-- End -->
            <p>{{ $product->links() }}</p>
            <!-- FOOTER -->
            <hr>
            <footer class="container">
                <p class="float-right"><a href="#">Back to top</a></p>
                <p>© 2019 Apple 2 Hand Company . inc</p>
                <h3>ติดต่อผู้พัฒนา</h3>
                <h5>Facebook : Watcharapong Nasaree</h5>
                <h5>Facebook : Toranit Wongkamsa</h5>
                <h5>Facebook : Butratthep Sarot</h5>
            </footer>

        </div>
    </div>
</main>
<script>
    $(document).ready(function() {
        $('.check').on('submit', function() {
            if (confirm('ต้องการทำรายการต่อหรือไม่')) {
                return true;
            } else {
                return false;
            }
        });
    });
</script>
@endsection