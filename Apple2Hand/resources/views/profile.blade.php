@extends('layouts.app2hand')

@section('content')
            <br>
            <br>
            <br>
<style>
   .f-size{
       font-size : 20px;
   }
   .l-size{
       font-size : 17px;
       background:pink;
   }
</style>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h3>My Profile</h3> <br>
       
            <img src="/uploads/avatars/{{ $profile->avatar }}" style="width:150px; height:150px; float:left; border-radius:50%; margin-right:25px;">
            
            <h4>Hello {{ $profile->first_name }} {{ $profile->last_name }}</h4>
            
            <form class="update" enctype="multipart/form-data" action="/profile" method="POST">
                <label>Update Profile Image</label>
                <input type="file" name="avatar">
                <input type="hidden" name="_token" value="{{ csrf_token() }}"><br>
                <button type="submit" class="btn btn-primary">อัพเดท</button>
                <!-- <input type="submit" class="pull-right btn btn-sm btn-primary" value="อัพเดท" > -->
            </form>
            @if (\Session::has('success'))
            <br><br><br>
                <div class="alert alert-success">
                    <p>{{ \Session::get('success') }}</p>
                </div>
            @endif

            @if ($errors->any())
            <br><br><br>
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            
            <br>
            <br>
            <br>
            <hr>

            <h3 align="center">ข้อมูลส่วนตัว</h3>   
            <br> 
            <p>
                <label class="f-size">ชื่อ :</label> &emsp; <label class="l-size">{{ $profile->first_name }}</label><br>
                <label class="f-size">นามสกุล :</label> &emsp; <label class="l-size">{{ $profile->last_name }}</label><br>
                <label class="f-size">เพศ :</label> &emsp; <label class="l-size">{{ $profile->gender }}</label><br>
                <label class="f-size">เบอร์โทรศัพท์ :</label> &emsp; <label class="l-size">{{ $profile->tel }}</label><br>
                <label class="f-size">หมายเลขบัตรประจำตัว :</label> &emsp; <label class="l-size">ไม่เปิดเผย</label><br>
                <label class="f-size">ที่อยู่ :</label> &emsp; <label class="l-size">{{ $profile->address }}</label><br>
                <label class="f-size">อายุ :</label> &emsp; <label class="l-size">{{ $profile->age }}</label><br>
            </p>
            <br>
            
            <br>
            <hr>

        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('.update').on('submit' , function(){
            if(confirm('คุณต้องการเปลี่ยนรูปโปรไฟล์ใช่หรือไม่')){
                return true;
            } else {
                return false;
            }
        });
    });
</script>
@endsection
