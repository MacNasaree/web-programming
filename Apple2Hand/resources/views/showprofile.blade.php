@extends('layouts.app2hand')

@section('content')
<br><br><br><br><br>

<style>
    body { 
    background-image: url('http://wallpapercanyon.com/wp-content/uploads/2016/01/Silver-Wallpaper-16.png');
    background-repeat: no-repeat;
    background-attachment: fixed;
    background-position: center top; 
    background-size: cover;
    }
    img {
    display: block;
    margin-left: auto;
    margin-right: auto;
    }
</style>

<div class="container">
    <!-- container -->

    <img src="/uploads/avatars/{{ $profile->avatar }}" style="width:200px; height:200px; border-radius:50%;"><br>
    <p>
        <h3 align="center">{{ $profile->first_name }} {{ $profile->last_name }}</h3>
         <h5 align="center">เพศ : {{ $profile->gender }}</h5>
        <h5 align="center">อายุ : {{ $profile->age }}</h5> 
    </p>

    <hr noshade>
    <h1 align="center">ติดต่อ</h1>
    <p>    
        <h5 align="center"><i class='fas fa-phone-volume' style='font-size:32px;color:red'></i> โทร : {{ $profile->tel }}</h5>
        <h5 align="center"><i class='fab fa-facebook' style='font-size:32px;color:blue'></i> Facebook : {{ $profile->facebook }}</h5>
        <h5 align="center"><i class='fab fa-line' style='font-size:32px;color:green'></i> Line : {{ $profile->line }}</h5>
    </p>
    
    

</div> <!-- end container -->

@endsection