<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Apple2Hand</title>
    <!-- CSS -->
    <link rel="stylesheet" href="{{ asset('bootstrap-4.3.1-dist/css/bootstrap.min.css') }}">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Jquery -->
    <!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> -->
    <script src="{{ asset('js/jquery-3.4.0.min.js') }}"></script>

    <!-- JS -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script> -->
    <!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script> -->
    <script src="{{ asset('bootstrap-4.3.1-dist/js/bootstrap.min.js') }}"></script>
    <!-- <script src="{{ asset('bootstrap-4.3.1-dist/js/bootstrap.bundle.min.js') }}"></script> -->
    <style>
        #c {
            background: green;
        }

        body {
            background-image: url('http://wallpapercanyon.com/wp-content/uploads/2016/01/Silver-Wallpaper-16.png');
            background-color: rgb(250, 250, 250);

        }
    </style>

    <!-- Icon -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

</head>

<body>
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <!-- navbar -->
        <div class="container">
            <img src="http://clipart-library.com/images/dT9Kjoe6c.png" style="width:32px; height:32px; top:10px; left:10px;">

            <a class="navbar-brand" href="{{ url('/') }}">Apple 2 Hand</a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample07" aria-controls="navbarsExample07" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarsExample07">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ url('/product') }}">หน้าแรก <span class="sr-only">(current)</span></a>
                    </li>
                </ul>

                @guest
                <ul class="navbar-nav my-2 my-md-0">
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ route('login') }}">Login <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ route('register') }}">Register <span class="sr-only">(current)</span></a>
                    </li>
                </ul>
                @else
                <ul class="navbar-nav my-2 my-md-0">
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ url('/product/create') }}">ลงขายสินค้า<span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ url('/topupcoin') }}">เติมCoin <span class="sr-only">(current)</span></a>
                    </li>
                   
                    <li class="nav-item active">
                        <a class="nav-link" id="c">Coin = {{ App\Coin::where('user_id', Auth::user()->user_id)->first()->coin }} Bath<span class="sr-only">(current)</span></a>
                    </li>
                </ul>
                <ul class="navbar-nav my-2 my-md-0">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="dropdown07" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="position:relative; padding-left:50px;">
                            <img src="/uploads/avatars/{{ App\Profile::where('user_id', Auth::user()->user_id)->first()->avatar }}" style="width:32px; height:32px; position:absolute; top:5px; left:10px; border-radius:50%">
                            {{ App\Profile::where('user_id', Auth::user()->user_id)->first()->first_name }} {{ App\Profile::where('user_id', Auth::user()->user_id)->first()->last_name }}<span class="caret"></span>
                        </a>

                        <div class="dropdown-menu" aria-labelledby="dropdown07">
                            <a class="dropdown-item" href="{{ url('/profile') }}">Profile</a>
                            <hr>
                            <a class="dropdown-item" href="{{ url('/buyhistory') }}">ประวัติการซื้อ</a>
                            <a class="dropdown-item" href="{{ url('/salehistory') }}">ประวัติการขาย</a>
                            <hr>
                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>

                        </div>
                    </li>
                </ul>
                @endguest
            </div>


        </div>
    </nav> <!-- end navbar -->

    <!-- <main class="py-4"> -->
    <main>
        @yield('content')
    </main>
</body>

</html>