@extends('layouts.app2hand')

@section('content')
<br>
<br>
<br>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        <!-- ===== Start Form ===== -->
                        @csrf

                        <div class="form-group row">
                            <!-- Email Address -->
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <!-- Password -->
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required autocomplete="new-password">

                                @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <!-- Confirm Password -->
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <hr noshade>
                        <h5 align="center">ข้อมูลส่วนตัว</h5><br> <!-- ============== ข้อมูลส่วนตัว =============== -->

                        <div class="form-group row">
                            <!-- First Name -->
                            <label for="first_name" class="col-md-4 col-form-label text-md-right">{{ __('ชื่อ') }}</label>

                            <div class="col-md-6">
                                <input id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name') }}" required>
                                @if ($errors->has('first_name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('first_name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <!-- Last Name -->
                            <label for="last_name" class="col-md-4 col-form-label text-md-right">{{ __('นามสกุล') }}</label>

                            <div class="col-md-6">
                                <input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ old('last_name') }}" required>
                                @if ($errors->has('last_name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('last_name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <!-- Gender -->
                            <label for="last_name" class="col-md-4 col-form-label text-md-right">{{ __('เพศ') }}</label>

                            <div class="col-md-6">
                                <input id="gender" type="radio" name="gender" value="ชาย" required> ชาย
                                <input id="gender" type="radio" name="gender" value="หญิง" required> หญิง
                            </div>
                        </div>

                        <div class="form-group row">
                            <!-- Telephone -->
                            <label for="tel" class="col-md-4 col-form-label text-md-right">{{ __('หมายเลขโทรศัพท์') }}</label>

                            <div class="col-md-6">
                                <input id="tel" type="text" class="form-control{{ $errors->has('tel') ? ' is-invalid' : '' }}" name="tel" value="{{ old('tel') }}" required>
                                @if ($errors->has('tel'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('tel') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <!-- ID card number -->
                            <label for="id_card_number" class="col-md-4 col-form-label text-md-right">{{ __('หมายเลขบัตรประชาชน') }}</label>

                            <div class="col-md-6">
                                <input id="id_card_number" type="text" class="form-control{{ $errors->has('id_card_number') ? ' is-invalid' : '' }}" name="id_card_number" value="{{ old('id_card_number') }}" required>
                                @if ($errors->has('id_card_number'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('id_card_number') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <!-- Address -->
                            <label for="address" class="col-md-4 col-form-label text-md-right">{{ __('ที่อยู่') }}</label>

                            <div class="col-md-6">
                                <textarea id="address" type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" value="{{ old('address') }}" required></textarea>
                                @if ($errors->has('address'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('address') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <!-- Age -->
                            <label for="age" class="col-md-4 col-form-label text-md-right">{{ __('อายุ') }}</label>

                            <div class="col-md-6">
                                <input id="age" type="text" class="form-control{{ $errors->has('age') ? ' is-invalid' : '' }}" name="age" value="{{ old('age') }}" required>
                                @if ($errors->has('age'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('age') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <!-- Facebook -->
                            <label for="facebook" class="col-md-4 col-form-label text-md-right">{{ __('Facebook Name') }}</label>

                            <div class="col-md-6">
                                <input id="facebook" type="text" class="form-control{{ $errors->has('facebook') ? ' is-invalid' : '' }}" name="facebook" value="{{ old('facebook') }}" required>
                                @if ($errors->has('facebook'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('facebook') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <!-- Line -->
                            <label for="line" class="col-md-4 col-form-label text-md-right">{{ __('Line ID') }}</label>

                            <div class="col-md-6">
                                <input id="line" type="text" class="form-control{{ $errors->has('line') ? ' is-invalid' : '' }}" name="line" value="{{ old('line') }}" required>
                                @if ($errors->has('line'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('line') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <!-- ปุ่ม Register -->
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>

                    </form> <!-- ===== End Form ===== -->
                </div>

            </div>
        </div>
    </div>
</div>

@endsection