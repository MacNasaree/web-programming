<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Apple2Hand</title>

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- CSS -->
    <link rel="stylesheet" href="{{ asset('bootstrap-4.3.1-dist/css/bootstrap.min.css') }}">

    <!-- Custom styles for this template -->
    <link href="{{ asset('docs/4.3/examples/carousel/carousel.css') }}" rel="stylesheet">

    <!-- Jquery -->
    <script src="{{ asset('js/jquery-3.4.0.min.js') }}"></script>

    <!-- JS -->
    <script src="{{ asset('bootstrap-4.3.1-dist/js/bootstrap.min.js') }}"></script>

</head>
<body>
    <header>
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <div class="container">
            <img src="http://clipart-library.com/images/dT9Kjoe6c.png" style="width:64px; height:64px; top:10px; left:10px;" >
            <a class="navbar-brand" href="/"><h1>Apple 2 Hand</h1></a>
            &emsp;&emsp;&emsp;&emsp;
            
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample07" aria-controls="navbarsExample07" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarsExample07">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="btn btn-lg btn-warning" href="{{ url('/product') }}" role="button">เข้าสู่เว็บไซต์</a>
                    </li>
                </ul>
                @guest
                <ul class="navbar-nav my-2 my-md-0">
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ route('login') }}">Login <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ route('register') }}">Register <span class="sr-only">(current)</span></a>
                    </li>
                </ul>
                @else
                <ul class="navbar-nav my-2 my-md-0">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="dropdown07" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="position:relative; padding-left:50px;">
                            <img src="/uploads/avatars/{{ App\Profile::where('user_id', Auth::user()->user_id)->first()->avatar }}" style="width:32px; height:32px; position:absolute; top:5px; left:10px; border-radius:50%">
                            {{ App\Profile::where('user_id', Auth::user()->user_id)->first()->first_name }} {{ App\Profile::where('user_id', Auth::user()->user_id)->first()->last_name }}<span class="caret"></span>
                        </a>

                        <div class="dropdown-menu" aria-labelledby="dropdown07">
                            <a class="dropdown-item" href="{{ url('/profile') }}">Profile</a>
                            <hr>
                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>

                        </div>
                    </li>
                </ul>
                @endguest
            </div>

        </div>
    </nav>
    </header>

    <main role="main">

        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1" class=""></li>
            <li data-target="#myCarousel" data-slide-to="2" class=""></li>
            </ol>
            <div class="carousel-inner">
            <div class="carousel-item active">
                <svg class="bd-placeholder-img" width="100%" height="100%" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img"><rect width="100%" height="100%" fill="#777"></rect></svg>
                <div class="container">
                <div class="carousel-caption text-left">
                    <img src="https://img.purch.com/o/aHR0cDovL3d3dy5sYXB0b3BtYWcuY29tL2ltYWdlcy93cC9wdXJjaC1hcGkvaW5jb250ZW50LzIwMTgvMTEvMTU0MzU4NzkxMzgxMC02MjB4NDAwLnBuZw==" width="300px" height="200px">
                    <img src="https://icdn2.digitaltrends.com/image/macbook-air-2018-focus-1500x1000.jpg" width="300px" height="200px">
                    <img src="https://www.thestar.com.my/~/media/online/2018/11/30/13/52/macbookair_dpa.ashx/?w=620&h=413&crop=1&hash=0B2B481FFDB2B4FB0B375F5BB5FE1CD26DC408F6" width="300px" height="200px">
                    <h1>Mac Book</h1>
                    <p>Mac Book มือสองราคาถูกๆค้าบ</p>
                    <p><a class="btn btn-lg btn-primary" href="{{ url('/product') }}" role="button">Go Shopping</a></p>
                </div>
                </div>
            </div>
            <div class="carousel-item">
                <svg class="bd-placeholder-img" width="100%" height="100%" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img"><rect width="100%" height="100%" fill="#777"></rect></svg>
                <div class="container">
                <div class="carousel-caption">
                    <img src="https://img.kaidee.com/prd/20171204/333530543/b/396f5e9b-6bf2-4a36-9974-e33f710c5f3a.jpg" width="300px" height="200px">
                    <img src="https://images.techhive.com/images/article/2016/03/iphone_se_review_mrv_010-37-100653394-large.jpg" width="300px" height="200px">
                    <img src="https://media1.nguoiduatin.vn/media/luong-duc-trong/2018/03/27/apple-phai-cat-giam-don-hang-oled-vi-iphone-x-e-am1.jpg" width="300px" height="200px">

                    <h1>Iphone</h1>
                    <p>Iphone มือสองราคาถูกๆค้าบ</p>
                    <p><a class="btn btn-lg btn-primary" href="{{ url('/product') }}" role="button">Go Shopping</a></p>
                </div>
                </div>
            </div>
            <div class="carousel-item">
                <svg class="bd-placeholder-img" width="100%" height="100%" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img"><rect width="100%" height="100%" fill="#777"></rect></svg>
                <div class="container">
                <div class="carousel-caption text-right">
                    <img src="http://image.coolz-server.com/s/c9GIBnhq" width="300px" height="200px">
                    <img src="http://www.krichhouse.com/wp-content/uploads/2017/11/DSC0020_resize.jpg" width="300px" height="200px">
                    <img src="http://cy.lnwfile.com/tdk7pf.jpg" width="300px" height="200px">
                    <h1>Ipad</h1>
                    <p>Ipad มือสองราคาถูกๆค้าบ</p>
                    <p><a class="btn btn-lg btn-primary" href="{{ url('product') }}" role="button">Go Shopping</a></p>
                </div>
                </div>
            </div>
            </div>
            <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
            </a>
        </div>
        <div class="container marketing">


    <!-- START THE FEATURETTES -->

    <!-- <hr class="featurette-divider"> -->

    <div class="row featurette">
      <div class="col-md-7">
        <h2 class="featurette-heading">Macbook มือสอง<span class="text-muted"> ซื้อง่ายขายคล่องที่เว็บเรา</span></h2>
        <p class="lead">มีสินค้าให้เลือกมากมาย  </p>
      </div>
      <div class="col-md-5">
      <img src="https://notebookspec.com/web/wp-content/uploads/2015/04/MacBook-Pro-Retina-13-2015-Review-021.jpg" width="550px" height="400px">
      </div>
    </div>

    <hr class="featurette-divider">


    <!-- /END THE FEATURETTES -->

  </div>

        
    </main>

</body>
</html>
