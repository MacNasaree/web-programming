<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->bigIncrements('user_id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('tel')->unique();
            $table->string('address');
            $table->string('age');
            $table->string('gender');
            $table->string('id_card_number')->unique();
            $table->string('facebook');
            $table->string('line');
            $table->string('avatar')->default('default.jpg');
            $table->timestamps();
        });
        
        DB::statement("ALTER TABLE profiles AUTO_INCREMENT = 10001;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
