<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    //
    protected $primaryKey = 'transaction_id';

    protected $fillable = [
        'transaction_id',
        'buyer',
        'seller',
        'category',
        'image',
        'title',
        'description',
        'price',
        'create_at',
        'update_at'
    ];
}
