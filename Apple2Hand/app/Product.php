<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $primaryKey = 'product_id';

    protected $fillable = [
        'product_id',
        'user_id',
        'category',
        'image',
        'title',
        'description',
        'price',
    ];
}
