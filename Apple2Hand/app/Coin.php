<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coin extends Model
{
    //
    protected $primaryKey = 'user_id';

    protected $fillable = [
        'user_id',
        'email',
        'coin'
    ];
}
