<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Profile extends Model
{
    //
    protected $primaryKey = 'user_id';
    
    protected $fillable = [
        'user_id',
        'first_name', 
        'last_name',
        'tel',
        'address',
        'age',
        'gender',
        'id_card_number',
        'avatar',
        'facebook',
        'line'
    ];
}
