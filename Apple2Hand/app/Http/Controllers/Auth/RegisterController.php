<?php

namespace App\Http\Controllers\Auth;

use App\Profile;
use App\User;
use App\Coin;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/product';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $tel_length = strlen((string)$data['tel']);
        $id_card_length = strlen((string)$data['id_card_number']);

        if ($tel_length == 10 && $id_card_length == 13 && is_numeric($data['tel']) && is_numeric($data['id_card_number']) ){
            return Validator::make($data, [
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
                
                // ข้อมูลส่วนตัว
                'first_name' => ['required','string','max:255'],
                'last_name' => ['required','string','max:255'],
                'tel' => ['required','string','unique:profiles'],
                'address' => ['required','string','max:255'],
                'age' => ['required','integer'],
                'gender' => ['required'],
                'id_card_number' => ['required','string','unique:profiles'],
                'facebook' => ['required'],
                'line' => ['required']
            ]);
        } elseif (!is_numeric($data['tel'])){
            return Validator::make($data, [
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],

                'tel' => ['required','integer','unique:profiles']
            ]);
        } elseif (!is_numeric($data['id_card_number'])){
            return Validator::make($data, [
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],

                'id_card_number' => ['required','integer','unique:profiles']
            ]);
        } elseif ($tel_length != 10){
            return Validator::make($data, [
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],

                'tel' => ['required','min:10','max:10','unique:profiles']
            ]);
        } elseif ($id_card_length != 13){
            return Validator::make($data, [
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],

                'id_card_number' => ['required','min:13','max:13','unique:profiles']
            ]);
        }

    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        $profiledata = new Profile([ // ============> profile
            'first_name'=>$data['first_name'],
            'last_name'=>$data['last_name'],
            'tel'=>$data['tel'],
            'address'=>$data['address'],
            'age'=>$data['age'],
            'gender'=>$data['gender'],
            'id_card_number'=>$data['id_card_number'],
            'facebook'=>$data['facebook'],
            'line'=>$data['line']
        ]);
        $profiledata->save();

        $coin_create = new Coin([ // ============> coin
            'email' => $data['email'],
        ]);
        $coin_create->save();

        return User::create([ // ============> user
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }
}
