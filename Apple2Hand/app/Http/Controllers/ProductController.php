<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Auth;
use Redirect;
use Image;
use App\Coin;
use App\Transaction;

class ProductController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
    }
 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() // ถ้าเข้า /product เฉยๆ จะมาทำใน index
    {
        //
        $product = Product::paginate(8);

        return view('home',compact('product'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() // ไปหน้าสร้างสินค้า /product/create
    {
        //
        if(!Auth::check()) {

            return Redirect::route('login');

        }else{

            return view('createproduct');

        }
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) // ถ้ามี post ส่งมาจะเข้าตรงนี้ สร้างสินค้า สร้าง Product
    {
        //
 
        $this->validate($request,[
            'category'=>'required',
            'title'=>'required',
            'description'=>['required','max:255'],
            'price' => ['required','integer'],
        ]); // ตรวจสอบ
        
        if ($request->hasFile('image')) {
            $avatar = $request->file('image');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(1600, 1200)->save(public_path('/uploads/product_image/' . $filename));
        }else{
            $filename = 'default_product.jpg';
        }
        
        $createproduct = new Product([
            'user_id' => Auth::user()->user_id,
            'category'=> $request->get('category'),
            'image' => $filename,
            'title'=> $request->get('title'),
            'description' => $request->get('description'),
            'price' => $request->get('price'),
            ]); 

        $createproduct->save();

        // นำค่าที่รับมาจาก form มาสร้างเป็นก้อน object เซ็ตค่าตามฟิล แล้วส่งไปตามชื่อฟิลที่อยู่ใน model Product
              
        return redirect()->route('product.index')->with('success','ลงขายสินค้าเรียบร้อย');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) // กดเพิ่มเติมจะมาเข้าตรงนี้
    {
        //
        $product = Product::find($id);
        
        return view('showproduct',compact('product'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) // กดลบจะมาเข้าตรงนี้
    {
        //
        $product = Product::find($id);

        if(Auth::user()->user_id == $product->user_id)
        {
             $product->delete();
        }

        return redirect()->route('product.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) // กดซื้อจะมาเข้าตรงนี้
    {
        //
        $product = Product::find($id);

        $coin_buyer = Coin::where('user_id',Auth::user()->user_id)->first();
        $coin_seller = Coin::where('user_id', $product->user_id)->first();

        if ($product->price > $coin_buyer->coin) {
            
            return Redirect::route('topupcoin.index');
        }
        else{
            $coin_buyer->coin =(int)$coin_buyer->coin - (int)$product->price;
            $coin_buyer->save();

            $coin_seller->coin =(int)$coin_seller->coin + (int)$product->price;
            $coin_seller->save();

            $transaction = new Transaction([
                'buyer' => Auth::user()->user_id,
                'seller' => $product->user_id,
                'category' => $product->category,
                'image' => $product->image,
                'title' => $product->title,
                'description' => $product->description,
                'price' => $product->price,
            ]);
            $transaction->save();

            $product->delete();
            
            return redirect()->route('product.index');
        }
        
            
        // 
      
        
    }
}
