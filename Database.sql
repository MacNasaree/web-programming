-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 24, 2019 at 04:53 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `usertest`
--

-- --------------------------------------------------------

--
-- Table structure for table `bills`
--

CREATE TABLE `bills` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `coins`
--

CREATE TABLE `coins` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `coin` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `coins`
--

INSERT INTO `coins` (`user_id`, `email`, `coin`, `created_at`, `updated_at`) VALUES
(10001, 'g@gmail.com', 23358, '2019-04-23 03:26:36', '2019-04-23 03:57:57'),
(10002, 'a@gmail.com', 27642, '2019-04-23 03:31:50', '2019-04-23 03:57:57'),
(10003, 'king@gmail.com', 15600, '2019-04-23 04:00:47', '2019-04-23 04:42:15'),
(10004, 'MyMac@gmail.com', 24400, '2019-04-23 04:23:07', '2019-04-23 04:42:15');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(42, '2014_10_12_000000_create_users_table', 1),
(43, '2014_10_12_100000_create_password_resets_table', 1),
(44, '2019_04_19_122344_create_profiles_table', 1),
(45, '2019_04_20_040143_create_coins_table', 1),
(46, '2019_04_21_184541_create_products_table', 1),
(47, '2019_04_23_061256_create_bills_table', 1),
(48, '2019_04_23_062147_create_transactions_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default_product.jpg',
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `user_id`, `category`, `image`, `title`, `description`, `price`, `created_at`, `updated_at`) VALUES
(10004, '10003', 'iphone', 'default_product.jpg', 'MacBook Pro 13 ปี 2017 Core i5 RAM 8 SSD 256 สภาพนางฟ้า มีประกันศูนย์', 'ว้าวๆสภาพนางฟ้า MacBook Pro (13-inch, 2017, Two Thunderbolt 3 ports) อุปกรณ์ครบยกกล่อง มีประกันศูนย์ Apple 01 พฤศจิกายน ,2562 #ราคาพิเศษ 41,900 บาท', '41900', '2019-04-23 04:11:12', '2019-04-23 04:11:12'),
(10006, '10003', 'macbook', '1556018312.jpg', 'เครื่องใหม่มาก!!!', 'Macbook ประกันอหลือ เครื่องใหม่มาก', '35000', '2019-04-23 04:18:33', '2019-04-23 04:18:33'),
(10008, '10003', 'macbook', '1556018418.jpg', 'เครื่องใหม่ สภาพดี', 'Macbook สภาพดี เครื่องใหม่มาก', '35990', '2019-04-23 04:20:19', '2019-04-23 04:20:19'),
(10009, '10004', 'ipad', '1556018737.jpg', 'ipad mini2 32gb th', 'เครื่องศูนย์ไทย สภาพสวย96% รีเซตได้ อุปกรณ์ชุดชาร์ทใหม่ ติดฟิล์ม เครื่องใช้งานปกติตามที่แจ้ง สภาพตามรูปเลยจ้า', '5500', '2019-04-23 04:25:37', '2019-04-23 04:25:37'),
(10011, '10004', 'iphone', '1556018881.jpg', 'Iphone 8 Plus 64gb', 'สีทอง เครื่องไทย ยกกล่อง อุปกรณ์แท้ครบยกกล่อง ราคา : 18000 บาท \r\nสภาพ : ใช้งานได้ปกติทุกอย่าง , สภาพภายนอกตีไป 95% ตำหนิ : อาจมีรอยตามการใช้งาน', '15000', '2019-04-23 04:28:02', '2019-04-23 04:28:02');

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tel` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `age` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_card_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `line` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default.jpg',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`user_id`, `first_name`, `last_name`, `tel`, `address`, `age`, `gender`, `id_card_number`, `facebook`, `line`, `avatar`, `created_at`, `updated_at`) VALUES
(10001, 'asdasd', 'asdads', '2836472831', 'haskdhjkadh', '5123', 'ชาย', '3746253626663', 'Mackie', 'zz77yw', 'default.jpg', '2019-04-23 03:26:36', '2019-04-23 03:26:36'),
(10002, '123', '123123', '1238761236', 'dsaasdsda', '50', 'ชาย', '1231231233123', 'Frame', 'Dream', 'default.jpg', '2019-04-23 03:31:50', '2019-04-23 03:31:50'),
(10003, 'King', 'Thor', '0987654321', '144/8 Science UBU', '21', 'ชาย', '1435672389900', 'King', 'Thor', '1556017456.jpg', '2019-04-23 04:00:47', '2019-04-23 04:04:19'),
(10004, 'My', 'Mac', '0954321768', '21 warin ubon', '25', 'ชาย', '1543896748456', 'My Mac', '0944555443', 'default.jpg', '2019-04-23 04:23:06', '2019-04-23 04:23:06');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `transaction_id` bigint(20) UNSIGNED NOT NULL,
  `buyer` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seller` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default_product.jpg',
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`transaction_id`, `buyer`, `seller`, `category`, `image`, `title`, `description`, `price`, `created_at`, `updated_at`) VALUES
(1, '10002', '10001', 'ipad', 'default_product.jpg', '4444444444', '23333333', '234', '2019-04-23 03:32:09', '2019-04-23 03:32:09'),
(2, '10002', '10001', 'iphone', 'default_product.jpg', '2223333', 'ssssss', '23124', '2019-04-23 03:57:57', '2019-04-23 03:57:57'),
(3, '10003', '10004', 'macbook', '1556018794.jpg', 'ขายด่วน', 'ขายด่วน ร้อนเงิน', '15000', '2019-04-23 04:41:59', '2019-04-23 04:41:59'),
(4, '10003', '10004', 'ipad', '1556019032.jpg', 'Iphone สีทอง', 'สภาพยังสวยการใช้งานปกติทุกอย่าง\r\nแบตไม่เสื่อม เครื่องเดิมๆ', '5900', '2019-04-23 04:42:09', '2019-04-23 04:42:09'),
(5, '10003', '10004', 'iphone', '1556018940.jpg', 'Iphone 5', 'สภาพดี', '3500', '2019-04-23 04:42:16', '2019-04-23 04:42:16');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(10001, 'g@gmail.com', NULL, '$2y$10$QPxP7ICwR/8Ug8y/JLDFYOr.A0GGDyfEX8A2T9D0ZUXeWBeZ5x3uq', NULL, '2019-04-23 03:26:37', '2019-04-23 03:26:37'),
(10002, 'a@gmail.com', NULL, '$2y$10$fpQOwyzrBr1q30ICyM/yl.fzgCPXzsEm10UylJcr3eWPEHN9xnpvG', NULL, '2019-04-23 03:31:50', '2019-04-23 03:31:50'),
(10003, 'king@gmail.com', NULL, '$2y$10$FsxuMZtyXX4c7g0kvglgO.kRZWq9IpiohlUWLUe.35TtsHroIIuqi', NULL, '2019-04-23 04:00:47', '2019-04-23 04:00:47'),
(10004, 'MyMac@gmail.com', NULL, '$2y$10$Ky1Fs0aFoJWUiIDZbBOYTusqhoyOd2kFriNApP1TolRlrTM0JfzqC', NULL, '2019-04-23 04:23:07', '2019-04-23 04:23:07');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bills`
--
ALTER TABLE `bills`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coins`
--
ALTER TABLE `coins`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `coins_email_unique` (`email`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `profiles_tel_unique` (`tel`),
  ADD UNIQUE KEY `profiles_id_card_number_unique` (`id_card_number`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`transaction_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bills`
--
ALTER TABLE `bills`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `coins`
--
ALTER TABLE `coins`
  MODIFY `user_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10005;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10012;

--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `user_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10005;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `transaction_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10005;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
